//
//  stos.hpp
//  Lab3
//
//  Created by Wojciech Beker on 14.03.2016.
//  Copyright © 2016 Wojciech Beker. All rights reserved.
//

#ifndef stos_hpp
#define stos_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

class Stos {
private:
    int top;
    int rozmiar;
    int *kontener;
public:
    Stos(int rozmiar);
    void push(int wartosc);
    int stackTop();
    void pop();
    bool isEmpty();
    ~Stos() {
        delete[] kontener;
    }
};

#endif /* stos_hpp */
