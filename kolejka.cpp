//
//  kolejka.cpp
//  Lab3
//
//  Created by Wojciech Beker on 14.03.2016.
//  Copyright © 2016 Wojciech Beker. All rights reserved.
//

#include "kolejka.hpp"

Kolejka::Kolejka(int rozmiar) {
    if (rozmiar <= 0) {
        cout << "Rozmiar stosu musi byc wiekszy od zera\n";
    }
    kontener = new int [rozmiar];
    poczatek = -1;
    koniec = -1;
    this->rozmiar=rozmiar;
}

void Kolejka::pushKolejka(int wartosc) {
    if (koniec == rozmiar) {
        cout << "Kolejka przepelniona\n";
    }
    if (isEmpty()) {
        poczatek = 0;
        koniec = 0;
    }
    else {
        koniec = (koniec+1)%rozmiar;
    }
    kontener[koniec] = wartosc;
}

int Kolejka::kolejkaTop() {
    if (isEmpty()) {
        cout << "Kolejka jest pusta\n";
        return -1;
    }
    return kontener[poczatek];
}

void Kolejka::kolejkaPop() {
    if (isEmpty()) {
        cout << "Kolejka jest pusta\n";
    }
    else if (poczatek == koniec) {
        poczatek = -1;
        koniec = -1;
    }
    else {
        poczatek = (poczatek+1)%rozmiar;
    }
}

bool Kolejka::isEmpty() {
    return (poczatek == -1 && koniec == -1);
}

Kolejka::~Kolejka() {
    delete [] kontener;
}