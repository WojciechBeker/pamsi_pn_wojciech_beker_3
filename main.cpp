//
//  main.cpp
//  Lab3
//
//  Created by Wojciech Beker on 14.03.2016.
//  Copyright © 2016 Wojciech Beker. All rights reserved.
//

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <stack>
#include <queue>
#include <fstream>
#include <iostream>
#include <sys/time.h>

#include "stos.hpp"
#include "kolejka.hpp"

using namespace std;

const int ROZMIAR = 500000;
int tablica[ROZMIAR];

stack <int> s1;
stack <int> s2;
stack <int> s3;

/* Funkcja odczytywania z pliku danych losowych */
void odczytajZPlikuTekstowego(int tab[], int n);

/* Funkcja zapisuje dane do pliku tekstowego */
void zapisDoPliku ();
//Zmienic na tablice dynamiczna tworzona przy generowaniu danych

void wiezaHanoi(stack <int> &s1, stack <int> &s2, stack <int> &s3, int x, int y, char decyzja);

void printHanoi(stack <int> a, stack <int> b, stack <int> c, int rozmiar);

int main(int argc, const char * argv[]) {
    Stos stos = Stos(ROZMIAR);
    Kolejka kolejka = Kolejka(ROZMIAR);
    int wybor = 0;
    int x = 0;
    srand( time(NULL));
    timeval t1, t2;
    double elapsedTime;
    
    stack <int> stosSTL;
    queue <int> kolejkaSTL;
    
    odczytajZPlikuTekstowego(tablica, ROZMIAR);
    
    do {
        cout << "1-> Stos" << endl;
        cout << "2-> Kolejka" << endl;
        cout << "3-> Wieza Hanoi" << endl;
        cout << "0-> Koniec" << endl;
        
        cin >> wybor;
        
        switch (wybor) {
            case 1: {
                do {
                    cout << "Badanie stosu" << endl;
                    cout << "1-> Stos STL" << endl;
                    cout << "2-> Stos na tablicy" << endl;
                    cout << "10-> Wroc" << endl;
                    
                    cin >> wybor;
                    switch (wybor) {
                        case 1: {
                            do {
                                cout << "Stos STL" << endl;
                                cout << "1-> Zbadaj stos" << endl;
                                cout << "2-> Dodaj element" << endl;
                                cout << "3-> Usun element" << endl;
                                cout << "4-> Usun wszystkie elementy" << endl;
                                cout << "5-> Wypisz elementy" << endl;
                                cout << "10-> Wroc" << endl;
                                
                                cin >> wybor;
                                
                                switch (wybor) {
                                    case 1: {
                                        while (stosSTL.empty() != 1) {
                                            stosSTL.pop();
                                        }
                                        for (int i = 1000; i <= 1000000; i *=10) {
                                            if (i > 100000) {
                                                i = 500000;
                                            }
                                            gettimeofday(&t1, NULL);
                                            for (int j = 0; j <= i; j++) {
                                                stosSTL.push(tablica[i]);
                                            }
                                            gettimeofday(&t2, NULL);
                                            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
                                            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
                                            cout << "Dla " << i << ": " << elapsedTime << "ms" << endl;
                                            while (stosSTL.empty() != 1) {
                                                stosSTL.pop();
                                            }

                                        }
                                        break;
                                    }
                                    case 2: {
                                        cout << "Jaki element dodac: ";
                                        cin >> x;
                                        cout << endl;
                                        stosSTL.push(x);
                                        break;
                                    }
                                    case 3: {
                                        cout << "Usunieto: " << stosSTL.top() << endl;
                                        stosSTL.pop();
                                        break;
                                    }
                                    case 4: {
                                        while (stosSTL.empty() != 1) {
                                            stosSTL.pop();
                                        }
                                        break;
                                    }
                                    case 5: {
                                        stack <int> copyStosSTL;
                                        
                                        break;
                                    }
                                }

                            }
                            while (wybor != 10);
                            
                            break;
                        }
                        case 2: {
                            do {
                                cout << "Stos na tablicy" << endl;
                                cout << "1-> Zbadaj stos" << endl;
                                cout << "2-> Dodaj element" << endl;
                                cout << "3-> Usun element" << endl;
                                cout << "4-> Usun wszystkie elementy" << endl;
                                cout << "5-> Wypisz elementy" << endl;
                                cout << "10-> Wroc" << endl;
                                
                                cin >> wybor;
                                
                                switch (wybor) {
                                    case 1: {
                                        while (stos.isEmpty() == 0) {
                                            stos.Stos::pop();
                                        }

                                        for (int i = 1000; i <= 1000000; i *=10) {
                                            if (i > 100000) {
                                                i = 500000;
                                            }
                                            gettimeofday(&t1, NULL);
                                            for (int j = 0; j <= i; j++) {
                                                stos.Stos::push(tablica[i]);
                                            }
                                            gettimeofday(&t2, NULL);
                                            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
                                            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
                                            cout << "Dla " << i << ": " << elapsedTime << "ms" << endl;
                                            while (stos.isEmpty() == 0) {
                                                stos.Stos::pop();
                                            }
                                            
                                        }
                                        break;
                                    }
                                    case 2: {
                                        cout << "Jaki element dodac: ";
                                        cin >> x;
                                        cout << endl;
                                        stos.Stos::push(x);
                                        break;
                                    }
                                    case 3: {
                                        cout << "Usunieto: " << stos.stackTop() << endl;
                                        stos.Stos::pop();
                                        break;
                                    }
                                    case 4: {
                                        while (stos.isEmpty() == 0) {
                                            stos.Stos::pop();
                                        }
                                        break;
                                    }
                                    case 5: {
                                        Stos copyStos = Stos(ROZMIAR);
                                        while (stos.isEmpty() == 0) {
                                            copyStos.Stos::push(stos.stackTop());
                                            cout << stos.stackTop() << endl;
                                            stos.Stos::pop();
                                        }
                                        while (copyStos.isEmpty() == 0) {
                                            stos.Stos::push(copyStos.stackTop());
                                            copyStos.Stos::pop();
                                        }
                                        
                                        break;
                                    }
                                }
 
                            }
                            while (wybor != 10);
                            break;
                        }
                    }
                }
                while (wybor != 10);
                break;
            }
            case 2: {
                do {
                    cout << "Badanie kolejki" << endl;
                    cout << "1-> Kolejka STL" << endl;
                    cout << "2-> Kolejka na tablicy" << endl;
                    cout << "10-> Wroc" << endl;
                    
                    cin >> wybor;
                    switch (wybor) {
                        case 1: {
                            do {
                                cout << "Kolejka STL" << endl;
                                cout << "1-> Zbadaj kolejke" << endl;
                                cout << "2-> Dodaj element" << endl;
                                cout << "3-> Usun element" << endl;
                                cout << "4-> Usun wszystkie elementy" << endl;
                                cout << "5-> Wypisz elementy" << endl;
                                cout << "10-> Wroc" << endl;
                                
                                cin >> wybor;
                                
                                switch (wybor) {
                                    case 1: {
                                        while (kolejkaSTL.empty() != 1) {
                                            kolejkaSTL.pop();
                                        }
                                        for (int i = 1000; i <= 1000000; i *=10) {
                                            if (i > 100000) {
                                                i = 500000;
                                            }
                                            gettimeofday(&t1, NULL);
                                            for (int j = 0; j <= i; j++) {
                                                kolejkaSTL.push(tablica[i]);
                                            }
                                            gettimeofday(&t2, NULL);
                                            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
                                            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
                                            cout << "Dla " << i << ": " << elapsedTime << "ms" << endl;
                                            while (kolejkaSTL.empty() != 1) {
                                                kolejkaSTL.pop();
                                            }

                                        }
                                        break;
                                    }
                                    case 2: {
                                        cout << "Jaki element dodac: ";
                                        cin >> x;
                                        cout << endl;
                                        kolejkaSTL.push(x);
                                        break;
                                    }
                                    case 3: {
                                        cout << "Usunieto: " << kolejkaSTL.front() << endl;
                                        kolejkaSTL.pop();
                                        break;
                                    }
                                    case 4: {
                                        while (kolejkaSTL.empty() != 1) {
                                            kolejkaSTL.pop();
                                        }
                                        break;
                                    }
                                    case 5: {
                                        if (kolejkaSTL.empty()) {
                                            cout << "Kolejka jest pusta" << endl;
                                        }
                                        else {
                                            int bufor = 0;
                                            for (int i = 0; i < kolejkaSTL.size(); i++) {
                                                bufor = kolejkaSTL.front();
                                                cout << bufor << endl;
                                                kolejkaSTL.pop();
                                                kolejkaSTL.push(bufor);
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            while (wybor != 10);
                            
                        break;
                        }
                        case 2: {
                            do {
                                cout << "Kolejka na tablicy" << endl;
                                cout << "1-> Zbadaj kolejke" << endl;
                                cout << "2-> Dodaj element" << endl;
                                cout << "3-> Usun element" << endl;
                                cout << "4-> Usun wszystkie elementy" << endl;
                                cout << "5-> Wypisz elementy" << endl;
                                cout << "10-> Wroc" << endl;
                                
                                cin >> wybor;
                                
                                switch (wybor) {
                                    case 1: {
                                        while (kolejka.Kolejka::isEmpty() != 1) {
                                            kolejka.kolejkaPop();
                                        }
                                        for (int i = 1000; i <= 1000000; i *=10) {
                                            if (i > 100000) {
                                                i = 500000;
                                            }
                                            gettimeofday(&t1, NULL);
                                            for (int j = 0; j <= i; j++) {
                                                kolejka.pushKolejka(tablica[i]);
                                            }
                                            gettimeofday(&t2, NULL);
                                            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
                                            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
                                            cout << "Dla " << i << ": " << elapsedTime << "ms" << endl;
                                            while (kolejka.Kolejka::isEmpty() != 1) {
                                                kolejka.kolejkaPop();
                                            }
                                            
                                        }
                                        break;
                                    }
                                    case 2: {
                                        cout << "Jaki element dodac: ";
                                        cin >> x;
                                        cout << endl;
                                        kolejka.pushKolejka(x);
                                        break;
                                    }
                                    case 3: {
                                        cout << "Usunieto " << kolejka.kolejkaTop() << endl;
                                        kolejka.kolejkaPop();
                                        break;
                                    }
                                    case 4: {
                                        while (kolejka.Kolejka::isEmpty() != 1) {
                                            kolejka.kolejkaPop();
                                        }
                                        break;
                                    }
                                    case 5: {
                                        Kolejka copyKolejka = Kolejka(ROZMIAR);
                                        if (kolejka.isEmpty()) {
                                            cout << "Kolejka jest pusta" << endl;
                                        }
                                        else {
                                            while (kolejka.Kolejka::isEmpty() != 1) {
                                                cout << kolejka.kolejkaTop() << " ";
                                                copyKolejka.pushKolejka(kolejka.kolejkaTop());
                                                kolejka.kolejkaPop();
                                            }
                                          }
                                        cout << endl;
                                        while (copyKolejka.Kolejka::isEmpty() != 1) {
                                            kolejka.pushKolejka(copyKolejka.kolejkaTop());
                                            copyKolejka.kolejkaPop();
                                        }
                                        }
                                        break;
                                    }
                                }

                            
                            while (wybor != 10);
                            break;
                        }
                    }
                }
                while (wybor != 10);
                break;
            }
            case 3: {
                char decyzja;
                cout << "Wieza Hanoi" << endl;
                do {
                    cout << "Podaj ilosc elementow na wiezy z przedzialu 1-64: ";
                    cin >> x;
                    cout << endl;
                }
                while (x > 64 || x < 1);
                for (int i = x; i > 0; i--) {
                    s1.push(i);
                }
                do {
                    cout << "Czy pokazac kolejne etapy? (t/n)" << endl;
                    cin >> decyzja;
                    cout << endl;
                }
                while (decyzja != 't' && decyzja != 'n');
                gettimeofday(&t1, NULL);
                wiezaHanoi(s1, s2, s3, x, x, decyzja);
                gettimeofday(&t2, NULL);
                elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
                elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
                cout << "\nAlgorytm wykonano w: " << elapsedTime << "ms\n\n";
                cout << s3.size() << endl;
                if (s1.size() > 0) {
                    for (int i = s1.size(); i > 0; i--) {
                        s1.pop();
                    }
                }
                if (s2.size() > 0) {
                    for (int i = s2.size(); i > 0; i--) {
                        s2.pop();
                    }
                }
                if (s3.size() > 0) {
                    for (int i = s3.size(); i > 0; i--) {
                        s3.pop();
                    }
                }
                break;
            }
        }
    }
    while (wybor != 0);
    return 0;
}

//ODCZYTAJ Z PLIKU---------------------------------------------------------------
void odczytajZPlikuTekstowego(int tab[], int n) {
    ifstream plik;
    plik.open("plik.txt");
    if (plik.good()) {
        cout << "Otworzono pomyslnie plik z danymi" << endl;
        for(int i = 0; i < n; i++) {
            plik >> tablica[i];
        }
        if (tablica[0] == 0 && tablica[1] == 0 && tablica[2] == 0) {
            cout << "Plik jest pusty" << endl << "Wypelniam plik danymi i wczytuje dane" << endl;
            //plik.close();
            zapisDoPliku();
            odczytajZPlikuTekstowego(tablica, ROZMIAR);
        }
    }
    else {
        cout << "Nie udalo sie otworzyc pliku" << endl << "Tworze nowy plik" << endl;
        //plik.close();
        zapisDoPliku();
        odczytajZPlikuTekstowego(tablica, ROZMIAR);
    }
    cout << endl;
    plik.close();
}

//ZAPIS DO PLIKU-------------------------------------------------------------------
void zapisDoPliku () {
    for(int i = 0;i < ROZMIAR;i++) {
        tablica[i] = (rand() % (101));
    }

    ofstream plik;
    plik.open("plik.txt");
    for(int i = 0; i < ROZMIAR; i++)
    {
        plik << tablica[i] << endl;
    }
    plik.close();
}

//WIEZA HANOI------------------------------------------------------------------------
void wiezaHanoi(stack <int> &a, stack <int> &b, stack <int> &c, int x, int y, char decyzja) {
    if (decyzja == 't') {
        printHanoi(s1, s2, s3, y);
        cout << endl;
    }
    if (x > 0) {
        wiezaHanoi(a, c, b, x-1, y, decyzja);
        c.push(a.top());
        a.pop();
        wiezaHanoi(b, a, c, x-1, y,decyzja);
    }
}

//WYPISUJE WIEZE HANOI--------------------------------------------------------------
void printHanoi(stack <int> a, stack <int> b, stack <int> c, int rozmiar) {
    for (int i = rozmiar; i > 0; i--) {
//----------------------------------------------
        if (a.size() == i) {
            if (a.top() > 9) {
                cout << a.top() << "         ";
            }
            else {
                cout << a.top() << "          ";
            }
            //copyA.push(a.top());
            a.pop();
        }
        else
            cout << "|          ";
//----------------------------------------------
        if (b.size() == i) {
            if (b.top() > 9) {
                cout << b.top() << "         ";
            }
            else {
                cout << b.top() << "          ";
            }
            b.pop();
        }
        else
            cout << "|          ";
//----------------------------------------------
        if (c.size() == i) {
            if (c.top() > 9) {
                cout << c.top() << "         ";
            }
            else {
                cout << c.top() << "          ";
            }
            c.pop();
        }
        else
            cout << "|          ";
        cout << endl;
    }
    
}