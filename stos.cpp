//
//  stos.cpp
//  Lab3
//
//  Created by Wojciech Beker on 14.03.2016.
//  Copyright © 2016 Wojciech Beker. All rights reserved.
//

#include "stos.hpp"

Stos::Stos(int rozmiar) {
    if (rozmiar <=0) {
        cout << "Rozmiar stosu musi byc wiekszy od zera\n";
    }
    kontener = new int [rozmiar];
    this->rozmiar=rozmiar;
    top=-1;
}

void Stos::push(int wartosc) {
    if (top == rozmiar) {
        cout << "Stos przepelniony\n";
    }
    else {
        top++;
        kontener[top] = wartosc;
    }
}

int Stos::stackTop() {
    if (top == -1) {
        cout << "Stos jest pusty\n";
    }
    return kontener[top];
}

void Stos::pop() {
    if (top == -1) {
        cout << "Stos jest pusty\n";
    }
    else
        top--;
}

bool Stos::isEmpty() {
    return (top == -1);
}