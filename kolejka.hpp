//
//  kolejka.hpp
//  Lab3
//
//  Created by Wojciech Beker on 14.03.2016.
//  Copyright © 2016 Wojciech Beker. All rights reserved.
//

#ifndef kolejka_hpp
#define kolejka_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

class Kolejka {
private:
    int poczatek;
    int koniec;
    int rozmiar;
    int *kontener;
public:
    Kolejka(int rozmiar);
    void pushKolejka(int wartosc);
    int kolejkaTop(); //Mimo nazwy kolejkaTop funkcja pobiera z poczatku kolejki a nie konca
    void kolejkaPop();
    bool isEmpty();
    ~Kolejka();
};

#endif /* kolejka_hpp */
